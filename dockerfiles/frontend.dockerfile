FROM node:14

WORKDIR /frontend

RUN npm install -g @vue/cli
RUN npm install vue-router

EXPOSE 8080

CMD npm run serve -- --port=8080
