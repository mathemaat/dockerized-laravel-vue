FROM php:7.4-fpm

RUN docker-php-ext-install pdo pdo_mysql

WORKDIR /backend

COPY --from=composer:1 /usr/bin/composer /usr/bin/composer

EXPOSE 8080

CMD php artisan serve --host=0.0.0.0 --port=8080
